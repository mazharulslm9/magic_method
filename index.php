<?php
require './Method.php';
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $foo = new Method();
        $a = $foo->test(5, 6, 7);
        echo '<pre>';
        print_r($a);
        echo '<br>';
        Method::xyz('1', 'qpc', 'test'); //__callstatic will be executed
        echo '<br>';
        $c1 = new Method();
        $c1->setName("Sunil");

        $c2 = clone $c1; //new object $c2 created

        $c2->setName("Vishal");

        echo $c1->getName() . "\n";
        echo $c2->getName() . "\n";
        echo '<br>';
       echo $app = $foo->jaman;//__get will triggerd
        echo '<br>';
       echo  $foo->test=1;//__set will triggerd
       echo '<br>';
       echo isset($foo->xyz);
          echo '<br>';
          unset($foo->x);
        ?>
    </body>
</html>
