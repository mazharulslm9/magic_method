<?php

/**
 * Description of Method
 *
 * @author Mazhar
 */
class Method {

    private $name;

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    public function __call($method, $args) {
        $x = array('a', 'b', 'c');
        print "Method $method called:\n";

        return $x;
    }

    public static function __callStatic($name, $arguments) {
        $a = print_r($arguments, true); //taking recursive array in string
        echo "__callStatic executed with name $name , parameter $a";
    }

    public function __clone() {
        $c = new Method();
        $c->setName($this->name);
        return $c;
    }

    public function __get($name) {
        echo "__get executed with name $name ";
        ;
    }

    public function __set($name, $value) {
        echo "__set executed with name = $name , value =$value";
    }
    public function __isset($name) {
        echo "__isset is called for $name";
    }
    public function __unset($name) {
        echo "__unset is called for $name";
    }
    public function __construct() {
        echo 'This is construct!!';;
    }
    public function __destruct() {
        echo 'This is destruct!!';;
    }
   

}
